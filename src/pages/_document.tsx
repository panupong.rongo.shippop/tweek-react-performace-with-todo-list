import * as React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'
import {
  RenderPage,
  DocumentInitialProps,
  DocumentContext,
} from 'next/dist/shared/lib/utils'
import createEmotionServer from '@emotion/server/create-instance'
import { EmotionCache } from '@emotion/cache'
import { EmotionCriticalToChunks } from '@emotion/server/types/create-instance'

import defaultTheme from '@themes/defaultTheme'
import createEmotionCache from '@themes/createEmotionCache'

import EmotionStyle from '../types/EmotionStyle'

export default class MyDocument extends Document {
  render(): JSX.Element {
    return (
      <Html lang="en">
        <Head>
          {/* PWA primary color */}
          <meta
            name="theme-color"
            content={defaultTheme.palette.primary.main}
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;400;500;600;700&display=swap"
            rel="stylesheet"
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Sarabun:wght@300;400;500;600;700&display=swap"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with static-site generation (SSG).
MyDocument.getInitialProps = async (context: DocumentContext) => {
  // Resolution order
  //
  // On the server:
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. document.getInitialProps
  // 4. app.render
  // 5. page.render
  // 6. document.render
  //
  // On the server with error:
  // 1. document.getInitialProps
  // 2. app.render
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. app.render
  // 4. page.render

  const originalRenderPage: RenderPage = context.renderPage

  // You can consider sharing the same emotion cache between all the SSR requests to speed up performance.
  // However, be aware that it can have global side effects.
  const cache: EmotionCache = createEmotionCache()

  const {
    extractCriticalToChunks,
  }: { extractCriticalToChunks: (html: string) => EmotionCriticalToChunks } =
    createEmotionServer(cache)

  // const {
  //   extractCriticalToChunks,
  // }: { extractCriticalToChunks: (html: string) => EmotionCriticalToChunks } =
  //   respOfCreateEmotionServer

  // context.renderPage = () =>
  //   originalRenderPage({
  //     enhanceApp:
  //       (App: AppType) =>
  //       (
  //         properties: React.PropsWithChildren<
  //           AppPropsType<NextRouter, Record<string, unknown>>
  //         >,
  //       ) =>
  //         <App emotionCache={cache} {...properties} />,
  //   })

  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  context.renderPage = () =>
    originalRenderPage({
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      enhanceApp: (App: any) => (properties: any) =>
        <App emotionCache={cache} {...properties} />,
    })

  const initialProperties: DocumentInitialProps =
    await Document.getInitialProps(context)
  // This is important. It prevents emotion to render invalid HTML.
  // See https://github.com/mui-org/material-ui/issues/26561#issuecomment-855286153
  const emotionStyles: EmotionCriticalToChunks = extractCriticalToChunks(
    initialProperties.html,
  )
  const emotionStyleTags: JSX.Element[] = emotionStyles.styles.map(
    (style: EmotionStyle) => (
      <style
        data-emotion={`${style.key} ${style.ids.join(' ')}`}
        key={style.key}
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: style.css }}
      />
    ),
  )

  return {
    ...initialProperties,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [
      ...React.Children.toArray(initialProperties.styles),
      ...emotionStyleTags,
    ],
  }
}
