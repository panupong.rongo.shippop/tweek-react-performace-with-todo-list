import * as React from 'react'
import Head from 'next/head'
import { AppProps } from 'next/app'
import { ThemeProvider } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import { CacheProvider, EmotionCache } from '@emotion/react'
import defaultTheme from '@themes/defaultTheme'
import createEmotionCache from '@themes/createEmotionCache'
import '@styles/global.css'

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache: EmotionCache = createEmotionCache()

interface MyAppProperties extends AppProps {
  emotionCache: EmotionCache
}

const App = ({
  Component,
  emotionCache = clientSideEmotionCache,
  pageProps,
}: MyAppProperties): JSX.Element => (
  <CacheProvider value={emotionCache}>
    <Head>
      <title>ultima-nextjs-express-typescripts</title>
      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width"
      />
    </Head>

    <ThemeProvider theme={defaultTheme}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
      <CssBaseline />
      <Component {...pageProps} />
    </ThemeProvider>
  </CacheProvider>
)
export default App
