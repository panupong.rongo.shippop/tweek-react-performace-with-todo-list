import React, { useState, useMemo } from 'react'

import { Box, Typography } from '@mui/material'
// interface Properties {}

const Dummy = (): JSX.Element => {
  const [myState] = useState('Dummy')

  const modifiedMyState: string = useMemo(
    () => `Modified ${myState}`,
    [myState],
  )
  return (
    <Box>
      <Typography>{`My State is => ${modifiedMyState}`}</Typography>
    </Box>
  )
}

export default Dummy
