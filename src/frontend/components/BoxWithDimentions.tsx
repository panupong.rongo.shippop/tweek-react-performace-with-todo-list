import React, { useRef, MutableRefObject } from 'react'
import { Box, Typography } from '@mui/material'
import useContainerDimensions from '@hooks/useContainerDimensions'

import ScreenDimention from '../../interfaces/ScreenDimention'

const BoxWithDimentions = (): JSX.Element => {
  const maReference: MutableRefObject<null> = useRef(null)
  const { width, height }: ScreenDimention = useContainerDimensions(maReference)
  return (
    <Box ref={maReference}>
      <Typography>Width: {width}</Typography>
      <Typography>Heigth: {height}</Typography>
    </Box>
  )
}

export default BoxWithDimentions
