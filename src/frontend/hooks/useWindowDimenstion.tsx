import { useState, useEffect } from 'react'
// import ScreenDimention from '../../interfaces/ScreenDimention'
import ScreenDimention from '../../interfaces/ScreenDimention'

const useWindowDimensions = (): ScreenDimention => {
  const [dimensions, setDimensions] = useState<ScreenDimention>({
    width: 0,
    height: 0,
  })

  useEffect(() => {
    const getWindowDimensions = (): ScreenDimention => {
      const {
        innerWidth: width,
        innerHeight: height,
      }: { innerWidth: number; innerHeight: number } = window
      return {
        width,
        height,
      }
    }
    const handleResize = (): void => {
      setDimensions(getWindowDimensions())
    }
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return dimensions
}
export default useWindowDimensions
