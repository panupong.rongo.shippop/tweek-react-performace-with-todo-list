type EmotionStyle = {
  key: string
  ids: string[]
  css: string
}

export default EmotionStyle
